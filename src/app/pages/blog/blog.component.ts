import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Blog } from 'src/app/interfaces/blog.interface';
import { BlogForm } from 'src/app/models/blog-form.model';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {


  blogs: Array<Blog> = [];
  blogForm!: FormGroup;

  validar = {
    titulo: [
      { type: 'required', message: 'El título' }
    ],
    autor: [
      { type: 'required', message: 'El autor' }
    ],
    contenido: [
      { type: 'required', message: 'El contenido es obligatorio' }
    ]
  };


  constructor(
    public blogService : BlogService , private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.obtenerBlogs();
    this.iniciarForm();

  }

  obtenerBlogs(){
    this.blogService.getBlogs()
      .subscribe((res) => {
        this.blogs = res;
        console.log(this.blogs);
      })
  }

  registrarBlog(){
    console.log(this.blogForm.value);
    if (this.blogForm.valid){
      this.blogService.crearBlog(this.blogForm.value)
        .subscribe(res => {
          console.log(res);
          this.obtenerBlogs();
        });
      this.blogForm.reset();

    }

  }

  iniciarForm(){
    this.blogForm = this.formBuilder.group({
      title: ['', Validators.required],
      autor: ['', Validators.required],
      resume: ['', Validators.required],
      num_comments: ['', Validators.required],
      imagen: ['', Validators.required],
    });
  }



}
