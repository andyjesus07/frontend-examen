export type BlogForm = {
  id: number,
  titulo: string
  autor: string
  contenido: string
  cantidadComents: number
  fecha: any
  img: any
}
