export type BlogForm = {
  titulo: string;
  autor: string;
  contenido: string;
  cantidadComents: number;
  img: string;
}
