import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Blog } from '../interfaces/blog.interface';
import { BlogForm } from '../models/blog-form.model';


@Injectable({
  providedIn: 'root'
})
export class BlogService {
  blog: Blog  = {};

  private URL = '/api/blog';

  constructor(
    private http: HttpClient,
  ) {
  }

  getBlogs() {
    return this.http.get<Array<Blog>>(this.URL);
  }

  crearBlog(blog: BlogForm){
    return this.http.post<string>(`/api/blog`,blog);
  }




}
